package edu.ntnu.stud;

import java.time.LocalTime;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashMap;
import java.util.NoSuchElementException;
import java.util.concurrent.atomic.AtomicReference;
import java.util.stream.Collectors;

/**
 * Main class for the train register object.
 * This class contains a hashmap of all the train departures.
 * Its main purpose is to store and manage the train departures.
 */
public class TrainRegister {
  
  /** Class Variables. */
  HashMap<Integer, TrainDeparture> register;
  LocalTime mainClock;
  
  /**
   * Constructor for the TrainRegister class.
   */
  public TrainRegister() {
    register = new HashMap<>();
    mainClock = LocalTime.of(0, 0);
  }
  
  
  /**
   * Get the size of the train register.
   *
   * @return size as an integer
   */
  public int trainRegisterSize() {
    return register.size();
  }
  
  /**
   * Returns the main clock as a string.
   *
   * @return The main clock as a string with format "HH:MM"
   */
  public String getMainClock() {
    String hours = String.valueOf(mainClock.getHour());
    String minutes = String.valueOf(mainClock.getMinute());
    
    if (hours.length() == 1) {
      hours = "0" + hours;
    }
    if (minutes.length() == 1) {
      minutes = "0" + minutes;
    }
    return String.format("%2s:%2s", hours, minutes);
  }
  
  
  /**
   * Sets the main clock.
   *
   * @param hours The hours of the main clock
   * @param minutes The minutes of the main clock
   */
  public void setMainClock(String hours, String minutes) {
    validateInput(hours, "hours");
    validateInput(minutes, "minutes");
    if (this.mainClock.isAfter(LocalTime.of(Integer.parseInt(hours), Integer.parseInt(minutes)))) {
      throw new IllegalArgumentException("The main clock cannot be set to a time in the past");
    } else {
      this.mainClock = LocalTime.of(Integer.parseInt(hours), Integer.parseInt(minutes));
      removeDeparturesBeforeTime(hours, minutes);
    }
  }

  
  /**
   * Adds a departure to the register with a track.
   *
   * @param trainNumber The train's unique number
   * @param line The line/route the train is allocated
   * @param destination The train's destination
   * @param hours The hours of the departure time
   * @param minutes The minutes of the departure time
   * @param track The track number the train will be stationed at
   *
   * @throws IllegalArgumentException if the train number already exists,
   *        or if the line and departure time already exist
   */
  public void addDeparture(
      String trainNumber,
      String line,
      String destination,
      String hours,
      String minutes,
      String track) {
    register.forEach((k, v) -> {
      if (k.toString().equals(trainNumber)) {
        throw new IllegalArgumentException("Train number already exists");
      }
      
      validateInput(hours, "hours");
      validateInput(minutes, "minutes");
      if ((v.getLine().equals(line))
          && (v.getDepartureTime()
            .equals(LocalTime.of(Integer.parseInt(hours), Integer.parseInt(minutes))))) {
        throw new IllegalArgumentException("Two departures assigned to the same line"
            + " can't leave at the same time");
      }
    });
    
    TrainDeparture train = new TrainDeparture(
        trainNumber, line, destination, hours, minutes, track);
    register.put(train.getTrainNumber(), train);
  }
  
  /**
   * Adds a departure to the register without a track.
   *
   * @param trainNumber The train's unique number
   * @param line The line/route the train is allocated
   * @param destination The train's destination
   * @param hours The hours of the departure time
   * @param minutes The minutes of the departure time
   *
   * @throws IllegalArgumentException if the train number already exists,
   *        or if the line and departure time already exist
   */
  public void addDeparture(String trainNumber, String line, String destination,
                       String hours, String minutes) {
    register.forEach((k, v) -> {
      if (k.toString().equals(trainNumber)) {
        throw new IllegalArgumentException("Train number already exists");
      }
      
      validateInput(hours, "hours");
      validateInput(minutes, "minutes");
      if ((v.getLine().equals(line))
          && (v.getDepartureTime()
            .equals(LocalTime.of(Integer.parseInt(hours), Integer.parseInt(minutes))))) {
        throw new IllegalArgumentException("Two departures assigned to the same line"
          + " can't leave at the same time");
      }
    });
    TrainDeparture train = new TrainDeparture(trainNumber, line, destination, hours, minutes);
    register.put(train.getTrainNumber(), train);
  }
  
  
  /**
   * Removes a departure from the register.
   *
   * @param trainNumber The train's unique number
   *
   * @throws IllegalArgumentException if the train number does not exist
   */
  public void removeDeparture(String trainNumber) {
    int refSize = register.size();
    register.entrySet().removeIf(entry -> entry.getKey().toString().equals(trainNumber));
    int newSize = register.size();
    if (refSize == newSize) {
      throw new IllegalArgumentException(
        "Train number does not exist, therefore no departure removed");
    }
  }
  
  
  /**
   * Sets the track number for a departure.
   *
   * @param trainNumber The number used to identify the train
   * @param track The track number the train will be stationed at
   *
   * @throws IllegalArgumentException if the train number does not exist, or if track is set
   *        to a train with the same arrival time and track
   */
  public void setTrackForDeparture(String trainNumber, String track) {
    validateInput(trainNumber, "trainNumber");
    validateInput(track, "track");
    
    if (!searchTrainNumber(trainNumber)) {
      throw new IllegalArgumentException("Train number does not exist");
    }
    
    TrainDeparture trainDeparture = register.get(Integer.parseInt(trainNumber));
    LocalTime departureTime = trainDeparture.getDelayedDepartureTime();
    
    boolean isSameTimeAndTrackOccupied = register.values().stream()
        .anyMatch(train ->
            train != trainDeparture
              && train.getDelayedDepartureTime().equals(departureTime)
              && train.getTrack() == Integer.parseInt(track));
    
    if (isSameTimeAndTrackOccupied) {
      throw new IllegalArgumentException("The track is used by another departure at this time");
    }
    
    register.get(Integer.parseInt(trainNumber)).setTrack(track);
  }
  
  /**
   * Sets the delay for a departure with a new departure time.
   *
   * @param trainNumber The number used to identify the train
   * @param hours The hours of the new time with delay
   * @param minutes The minutes of the new time with delay
   *
   * @throws IllegalArgumentException if the train number does not exist, or if
   *        the delayed departure time is interfering with another train at the same track
   */
  public void setDelayNewTimeForDeparture(String trainNumber, String hours, String minutes) {
    validateInput(trainNumber, "trainNumber");
    validateInput(hours, "hours");
    validateInput(minutes, "minutes");
    
    if (!searchTrainNumber(trainNumber)) {
      throw new IllegalArgumentException("Train number does not exist");
    }
    
    TrainDeparture trainDeparture = register.get(Integer.parseInt(trainNumber));
    int track = trainDeparture.getTrack();
    
    if (trainDeparture.getDepartureTime().isAfter(
        LocalTime.of(Integer.parseInt(hours), Integer.parseInt(minutes)))) {
      throw new IllegalArgumentException("Delay must be after the departure time");
    }
    
    boolean trackOccupiedAndLeavingSameTime = register.values().stream()
        .anyMatch(train ->
          train != trainDeparture
            && train.getDelayedDepartureTime()
              .equals(LocalTime.of(Integer.parseInt(hours), Integer.parseInt(minutes)))
            && train.getTrack() == track);
    
    if (trackOccupiedAndLeavingSameTime) {
      throw new IllegalArgumentException(
        "The track is used by another departure at this time. Please extend the delay");
    }
    
    register.get(Integer.parseInt(trainNumber)).setDelayNewTime(hours, minutes);
  }

  /**
   * Sets the delay for a departure with the deviation from the original time.
   *
   * @param trainNumber The number used to identify the train
   * @param minutes The minutes past the departure time, the delay
   *
   * @throws IllegalArgumentException if the train number does not exist, or if
   *        the delayed departure time is interfering with another train at the same track
   */
  public void setDelayDeviationForDeparture(String trainNumber, String minutes) {
    validateInput(trainNumber, "trainNumber");
    validateInput(minutes, "delay");
    
    if (!searchTrainNumber(trainNumber)) {
      throw new IllegalArgumentException("Train number does not exist");
    }
    
    TrainDeparture trainDeparture = register.get(Integer.parseInt(trainNumber));
    int track = trainDeparture.getTrack();
    
    boolean trackOccupiedAndLeavingSameTime = register.values().stream()
        .anyMatch(train ->
          train != trainDeparture
            && train.getDelayedDepartureTime()
              .equals(trainDeparture.getDepartureTime().plusMinutes(Integer.parseInt(minutes)))
            && train.getTrack() == track);
    
    if (trackOccupiedAndLeavingSameTime) {
      throw new IllegalArgumentException(
        "The track is used by another departure at this time. Please extend the delay");
    }
    
    register.get(Integer.parseInt(trainNumber)).setDelayDeviation(minutes);
  }
  
  
  /**
   * Returns a departure from the register.
   *
   * @param trainNumber The number used to identify the train
   * @return The departure with the train number
   *
   * @throws IllegalArgumentException if the train number does not exist
   */
  public TrainDeparture getDeparture(String trainNumber) {
    AtomicReference<TrainDeparture> departure = new AtomicReference<>();
    register.forEach((k, v) -> {
      if (k.toString().equals(trainNumber)) {
        departure.set(v);
      }
    });
    if (departure.get() == null) {
      throw new IllegalArgumentException("Departure does not exist");
    }
    return departure.get();
    
  }
  
  /**
   * Returns all departures to a specified destination.
   *
   * @param destination The destination of the departures
   * @return An arraylist of all the departures to the destination
   *
   * @throws IllegalArgumentException if there are no departures to the destination
   */
  public ArrayList<TrainDeparture> getDeparturesToDestination(String destination) {
    ArrayList<TrainDeparture> departures = new ArrayList<>();
    register.forEach((k, v) -> {
      if (v.getDestination().equalsIgnoreCase(destination)) {
        departures.add(v);
      }
    });
    if (departures.isEmpty()) {
      throw new IllegalArgumentException("No departures to  this destination");
    } else {
      departures.sort(Comparator.comparing(TrainDeparture::getDepartureTime));
    }
    return departures;
  }
  
  /**
   * Returns all departures in the register sorted by departure time.
   *
   * @return An arraylist of all the departures sorted by departure time
   *
   * @throws NoSuchElementException if there are no departures in the register
   */
  public ArrayList<TrainDeparture> getSortedDepartures() {
    if (register.isEmpty()) {
      throw new NoSuchElementException("No departures in register");
    }
    return register.values()
      .stream()
      .sorted(Comparator.comparing(TrainDeparture::getDepartureTime))
      .collect(Collectors.toCollection(ArrayList::new));
  }
  
  
  /**
   * Removes all departures before a specified time. Private because it is used in other methods.
   *
   * @param hours The hours of the specified time
   * @param minutes The minutes of the specified time
   */
  private void removeDeparturesBeforeTime(String hours, String minutes) {
    try {
      validateInput(hours, "hours");
      validateInput(minutes, "minutes");
    } catch (IllegalArgumentException e) {
      throw new IllegalArgumentException("Invalid time");
    }
    register.entrySet().removeIf(entry -> entry.getValue().getDepartureTime()
        .isBefore(LocalTime.of(Integer.parseInt(hours), Integer.parseInt(minutes))));
  }
  
  /**
   * Private method used to validate if a train number exists in the register.
   *
   * @param trainNumber The train number to find
   * @return A boolean value depicting if the train number already exist
   */
  private boolean searchTrainNumber(String trainNumber) {
    return register.keySet().stream().anyMatch(keys -> keys.toString().equals(trainNumber));
  }
  
  /**
   * Private method to validate inputs of the methods in this class.
   *
   * @param input The input to be validated
   * @param reference The reference to what the input is
   *
   * @throws IllegalArgumentException if the input is invalid for the reference
   */
  private void validateInput(String input, String reference) {
    switch (reference) {
      case "trainNumber":
        if (!Checks.checkTrainNumber(input)) {
          throw new IllegalArgumentException("The train number should only be a number"
            + " between 1-9999.");
        }
        break;
      case "line":
        if (!Checks.checkLine(input)) {
          throw new IllegalArgumentException("The format of the line is incorrect.\n"
            + "The line must be 3 characters long.");
        }
        break;
      case "destination":
        if (!Checks.checkDestination(input)) {
          throw new IllegalArgumentException("The destination should only use letters and spaces.");
        }
        break;
      case "track":
        if (!Checks.checkTrack(input)) {
          throw new IllegalArgumentException("The track number should only use a number"
            + " between 1-9.");
        }
        break;
      case "delay":
        int inputInt;
        try {
          inputInt = Integer.parseInt(input);
        } catch (NumberFormatException e) {
          throw new IllegalArgumentException("The delay should be a number that"
            + " represents minutes.");
        }
        if (inputInt < 0) {
          throw new IllegalArgumentException("The delay should be a positive number.");
        }
        break;
      case "hours":
        if (!Checks.checkHours(input)) {
          throw new IllegalArgumentException("The hours should only use a number between 0-23.");
        }
        break;
      case "minutes":
        if (!Checks.checkMinutes(input)) {
          throw new IllegalArgumentException("The minutes should only use a number between 0-59.");
        }
        break;
      default:
        throw new IllegalArgumentException("The reference is not valid.");
    }
  }
}
