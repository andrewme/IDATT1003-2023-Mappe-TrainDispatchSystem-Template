package edu.ntnu.stud;

import javax.swing.JOptionPane;

/**
 * Class to print menus.
 * The methods in this class are static,
 * so they can be called without creating an object of the class.
 * The methods are used to print menus for the UserInterface class.
 */
public class MenuPrinter {
  
  /**
   * Method to print the login menu.
   *
   * @return The user's input as a string
   */
  public static String loginMenu() {
    return JOptionPane.showInputDialog(null, """
      Welcome to the train station!
      
      Login?
      Y/n""", "Login", JOptionPane.PLAIN_MESSAGE);
  }
  
  /**
   * Method to print the main menu.
   *
   * @return The user's input as a string
   */
  public static String mainMenu() {
    return JOptionPane.showInputDialog(null, """
      Main Menu
      
      1. Show departures all departures
      2. Access the clock
      3. Access a departure
      4. Modify the register
      
      0. Exit""", "Main Menu", JOptionPane.PLAIN_MESSAGE);
  }
  
  /**
   * Method to print the clock menu.
   *
   * @return The user's input as a string
   */
  public static String menuClock() {
    return JOptionPane.showInputDialog(null, """
      Clock
      
      1. Get time
      2. Set time
      
      0. Go back""", "Main Clock", JOptionPane.PLAIN_MESSAGE);
  }

  /**
   * Method to print the menu for modifying a departure.
   *
   * @return The user's input as a string
   */
  public static String menuDepartures() {
    return JOptionPane.showInputDialog(null, """
      Departures
      
      1. Get a departure
      2. Set track for a departure
      3. Set delay for a departure
      
      0. Go back""", "Departures", JOptionPane.PLAIN_MESSAGE);
  }
  
  /**
   * Method to print the menu for modifying a departure's delay.
   *
   * @return The user's input as a string
   */
  public static String menuDelay() {
    return JOptionPane.showInputDialog(null, """
      Add Delay
      
      1. Set delay with new time
      2. Set delay with deviation
      
      0. Go back""", "Add Delay", JOptionPane.PLAIN_MESSAGE);
  }
  
  /**
   * Method to print the menu for modifying the register.
   *
   * @return The user's input as a string
   */
  public static String menuRegister() {
    return JOptionPane.showInputDialog(null, """
      Register
      
      1. Add departure
      2. Remove departure
      3. Get departures to a specified destination
      
      0. Go back""", "Train Register", JOptionPane.PLAIN_MESSAGE);
  }
}
