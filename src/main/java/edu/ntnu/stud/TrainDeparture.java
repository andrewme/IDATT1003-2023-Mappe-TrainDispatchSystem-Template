package edu.ntnu.stud;

import java.time.Duration;
import java.time.LocalTime;

/**
 * Main class for the train departure object.
 * This class contains all the information about a train's departure.
 *
 * @author André Merkesdal
 */
public class TrainDeparture {
 
  /** Class Variables. */
  private final LocalTime departureTime;
  private final String line;
  private final int trainNumber;
  private final String destination;
  private int track;
  private Duration delay;

  /**
   * Constructor for the TrainDeparture class with track number.
   *
   * @param trainNumber The train's unique number
   * @param line The line/route the train is allocated
   * @param destination The train's destination
   * @param hours The hours of the departure time
   * @param minutes The minutes of the departure time
   * @param track The track number the train will be stationed at
   */
  public TrainDeparture(String trainNumber, String line, String destination,
                        String hours, String minutes, String track) {
    
    validateInput(trainNumber, "trainNumber");
    validateInput(line, "line");
    validateInput(destination, "destination");
    validateInput(track, "track");
    validateInput(hours, "hours");
    validateInput(minutes, "minutes");
    
    this.trainNumber = Integer.parseInt(trainNumber);
    this.line = line;
    this.destination = destination;
    this.departureTime = LocalTime.of(Integer.parseInt(hours), Integer.parseInt(minutes));
    this.track = Integer.parseInt(track);
    this.delay = Duration.ofMinutes(0);
  }

  /**
   * Constructor for the TrainDeparture class without track number.
   *
   * @param trainNumber The train's unique number
   * @param line The line/route the train is allocated
   * @param destination The train's destination
   * @param hours The hours of the departure time
   * @param minutes The minutes of the departure time
   */
  public TrainDeparture(String trainNumber, String line, String destination,
                        String hours, String minutes) {
    
    validateInput(trainNumber, "trainNumber");
    validateInput(line, "line");
    validateInput(destination, "destination");
    validateInput(hours, "hours");
    validateInput(minutes, "minutes");
    
    this.trainNumber = Integer.parseInt(trainNumber);
    this.line = line;
    this.destination = destination;
    this.departureTime = LocalTime.of(Integer.parseInt(hours), Integer.parseInt(minutes));
    this.track = -1;
    this.delay = Duration.ofMinutes(0);
  }

  
  /**
   * Get the train number.
   *
   * @return trainNumber
   */
  public int getTrainNumber() {
    return trainNumber;
  }

  /**
   * Get the departure time.
   *
   * @return departureTime
   */
  public LocalTime getDepartureTime() {
    return departureTime;
  }

  /**
   * Get the line for the train.
   *
   * @return line
   */
  public String getLine() {
    return line;
  }

  /**
   * Get the destination the train is going to.
   *
   * @return destination
   */
  public String getDestination() {
    return destination;
  }

  /**
   * Get the track number.
   *
   * @return track
   */
  public int getTrack() {
    return track;
  }
  
  /**
   * Get the delay for the departure.
   *
   * @return delay
   */
  public Duration getDelay() {
    return delay;
  }

  /**
   * Get the delayed departure time.
   *
   * @return delayedDepartureTime
   */
  public LocalTime getDelayedDepartureTime() {
    return departureTime.plus(delay);
  }
  
  
  /**
   * Sets the track number for the train.
   *
   * @param track The track number to be set
   */
  public void setTrack(String track) {
    validateInput(track, "track");
    this.track = Integer.parseInt(track);
  }

  /**
   * Sets the delay for the train with the new time as input.
   *
   * @param hours Hours of the new departure time
   * @param minutes Minutes of the new departure time
   */
  public void setDelayNewTime(String hours, String minutes) {
    validateInput(hours, "hours");
    validateInput(minutes, "minutes");
    
    this.delay = Duration.between(departureTime, LocalTime.of(Integer.parseInt(hours),
      Integer.parseInt(minutes)));
  }

  /**
   * Sets the delayed departure time for the train with the deviation as input.
   *
   * @param minutes Minutes of the deviation
   */
  public void setDelayDeviation(String minutes) {
    validateInput(minutes, "delay");
    
    this.delay = Duration.ofMinutes(Integer.parseInt(minutes));
  }

  
  /**
   * Return the train departure as a string.
   *
   * @return String of the train departure
   */
  public String departureToString() {
    return String.format("||%-3s | %s %s %s |%s| %s ||",
      this.departureTime.toString(),
      Checks.checkLengthLine(this.line),
      Checks.checkLengthDestination(this.destination),
      Checks.checkLengthTrainNumber(this.trainNumber),
      Checks.checkLengthTrack(this.track),
      Checks.checkLengthDelay(this.delay));
  }
  
  
  /**
   * A private method to validate the inputs used to create and mutate the object.
   *
   * @param input The input to be validated
   * @param reference The reference to what the input is as a string
   *
   * @throws IllegalArgumentException if the input is invalid for the reference
   */
  private void validateInput(String input, String reference) {
    switch (reference) {
      case "trainNumber":
        if (!Checks.checkTrainNumber(input)) {
          throw new IllegalArgumentException("The train number should only be a number"
            + " between 1-9999.");
        }
        break;
      case "line":
        if (!Checks.checkLine(input)) {
          throw new IllegalArgumentException("The format of the line is incorrect.\n"
            + "The line must be 3 characters long.");
        }
        break;
      case "destination":
        if (!Checks.checkDestination(input)) {
          throw new IllegalArgumentException("The destination should only use letters and spaces.");
        }
        break;
      case "track":
        if (!Checks.checkTrack(input)) {
          throw new IllegalArgumentException("The track number should only use a number"
            + " between 1-9.");
        }
        break;
      case "delay":
        int inputInt;
        try {
          inputInt = Integer.parseInt(input);
        } catch (NumberFormatException e) {
          throw new IllegalArgumentException("The delay should be a number that"
            + " represents minutes.");
        }
        if (inputInt < 0) {
          throw new IllegalArgumentException("The delay should be a positive number.");
        }
        break;
      case "hours":
        if (!Checks.checkHours(input)) {
          throw new IllegalArgumentException("The hours should only use a number between 0-23.");
        }
        break;
      case "minutes":
        if (!Checks.checkMinutes(input)) {
          throw new IllegalArgumentException("The minutes should only use a number between 0-59.");
        }
        break;
      default:
        throw new IllegalArgumentException("The reference is not valid.");
    }
  }
}
