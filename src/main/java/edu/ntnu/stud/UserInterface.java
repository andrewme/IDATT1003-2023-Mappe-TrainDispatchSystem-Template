package edu.ntnu.stud;

import java.awt.Font;
import java.util.ArrayList;
import java.util.Objects;
import javax.swing.JOptionPane;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;

/**
 * Main class for the user interface. This class i responsible for the user's interaction with the
 * program, and will display the different menus and handle the user's input.
 *
 * @author André Merkesdal
 */
public class UserInterface {

  /** Class Variable. */
  private TrainRegister trainRegister;
  
  /**
   * Initiation of the program. This method will display the login menu and initiate the train
   * register.
   */
  public void init() {

    trainRegister = new TrainRegister();
    
    String input;
    do {
      input = MenuPrinter.loginMenu();
      try {
        if (input.equalsIgnoreCase("Y")) {
          break;
        } else if (input.equalsIgnoreCase("n")) {
          JOptionPane.showMessageDialog(null, """
            Exiting...
            
            Thank you for using the program""");
          System.exit(0);
        } else {
          JOptionPane.showMessageDialog(null, "Please type 'Y' or 'n'");
        }
      } catch (NullPointerException e) {
        JOptionPane.showMessageDialog(null, """
            Exiting...
            
            Thank you for using the program""");
        System.exit(0);
      }
    } while (true);
  }
  
  /**
   * Start of the program. This method will display the main menu and handle the user's input
   * further. The main part of the program.
   */
  public void start() {

    trainRegister.addDeparture("2453", "F22", "Oslo",
        "20", "20");
    trainRegister.addDeparture("603", "N4", "Trondheim",
        "15", "30");
    trainRegister.addDeparture("5578", "V53", "Bergen",
        "11", "05", "1");
    trainRegister.addDeparture("101", "RS4", "Stavanger",
        "10", "00", "2");
    
    do {
      try {
        switch (MenuPrinter.mainMenu()) {
          
          case SHOW_DEPARTURES -> {
            StringBuilder string = new StringBuilder();
  
            if (trainRegister.trainRegisterSize() == 0) {
              string.append("No departures registered");
            } else {
              ArrayList<TrainDeparture> sortedlist = trainRegister.getSortedDepartures();
              string.append(addHeaderDepartureTable());
              sortedlist.forEach(train -> string.append(train.departureToString()).append("\n"));
            }
            string.append("\nTime: ").append(trainRegister.getMainClock());
  
            Font font = new Font(Font.MONOSPACED, Font.PLAIN, 12);
            JTextArea textArea = new JTextArea(string.toString());
            textArea.setFont(font);
            textArea.setEditable(false);
            textArea.setLineWrap(true);
            
            JScrollPane scrollPane = new JScrollPane(textArea);
            scrollPane.setPreferredSize(new java.awt.Dimension(400, 200));
  
            JOptionPane.showMessageDialog(null, scrollPane);
          }
          
          case ACCESS_CLOCK -> {
            boolean returnToMainMenu = false;
            
            do {
              try {
                switch (MenuPrinter.menuClock()) {
                  
                  case GET_TIME -> JOptionPane.showMessageDialog(
                      null, trainRegister.getMainClock());
                  
                  case SET_TIME -> {
                    String hours = JOptionPane.showInputDialog("Hours of the time:");
                    String minutes = JOptionPane.showInputDialog("Minutes of the time:");
                    try {
                      trainRegister.setMainClock(hours, minutes);
                      JOptionPane.showMessageDialog(
                          null, "Time set to: " + trainRegister.getMainClock());
                    } catch (Exception e) {
                      JOptionPane.showMessageDialog(null, e.getMessage());
                    }
                  }
                  
                  case RETURN -> returnToMainMenu = true;
                  
                  default -> JOptionPane.showMessageDialog(
                      null, "Please type the number representing your choice");
                }
              } catch (NullPointerException e) {
                returnToMainMenu = true;
              }
            } while (!returnToMainMenu);
          }
          
          case ACCESS_DEPARTURES -> {
            boolean returnToMainMenu = false;
            
            do {
              try {
                switch (MenuPrinter.menuDepartures()) {
                  
                  case GET_DEPARTURE -> {
                    String trainNumber = JOptionPane.showInputDialog("Train number:");
                    StringBuilder string = new StringBuilder();
                    
                    try {
                      string.append(addHeaderDepartureTable());
                      string.append(trainRegister.getDeparture(trainNumber).departureToString());
                    } catch (Exception e) {
                      JOptionPane.showMessageDialog(null, e.getMessage());
                    }
                    
                    Font font = new Font(Font.MONOSPACED, Font.PLAIN, 12);
                    JTextArea textArea = new JTextArea(string.toString());
                    textArea.setFont(font);
                    textArea.setEditable(false);
                    textArea.setLineWrap(true);
                    JScrollPane scrollPane = new JScrollPane(textArea);
                    scrollPane.setPreferredSize(new java.awt.Dimension(400, 100));
                    JOptionPane.showMessageDialog(null, scrollPane);
                  }
                  
                  case SET_TRACK -> {
                    String trainNumber = JOptionPane.showInputDialog("Train number:");
                    String track = JOptionPane.showInputDialog("Track:");
                    try {
                      trainRegister.setTrackForDeparture(trainNumber, track);
                      JOptionPane.showMessageDialog(
                          null, "Track set to: "
                          + trainRegister.getDeparture(trainNumber).getTrack());
                    } catch (Exception e) {
                      JOptionPane.showMessageDialog(null, e.getMessage());
                    }
                  }
                  
                  case SET_DELAY -> {
                    boolean returnToMenuDepartures = false;
                    
                    do {
                      try {
                        switch (MenuPrinter.menuDelay()) {
                          
                          case SET_DELAY_NEW_TIME -> {
                            String trainNumber = JOptionPane.showInputDialog("Train number:");
                            String hours = JOptionPane.showInputDialog("Hours of delayed time:");
                            String minutes = JOptionPane.showInputDialog(
                                "Minutes of delayed time:");
                            
                            try {
                              trainRegister.setDelayNewTimeForDeparture(
                                  trainNumber, hours, minutes);
                              JOptionPane.showMessageDialog(
                                  null,
                                  "Delayed departure time set to: "
                                      + trainRegister
                                          .getDeparture(trainNumber)
                                          .getDelayedDepartureTime());
                            } catch (Exception e) {
                              JOptionPane.showMessageDialog(null, e.getMessage());
                            }
                          }
                          
                          case SET_DELAY_DEVIATION -> {
                            String trainNumber = JOptionPane.showInputDialog("Train number:");
                            String minutes = JOptionPane.showInputDialog("Minutes of delay:");
                            
                            try {
                              trainRegister.setDelayDeviationForDeparture(trainNumber, minutes);
                              JOptionPane.showMessageDialog(
                                  null,
                                  " Delayed departure time set to: "
                                      + trainRegister
                                          .getDeparture(trainNumber)
                                          .getDelayedDepartureTime());
                            } catch (Exception e) {
                              JOptionPane.showMessageDialog(null, e.getMessage());
                            }
                          }
                          
                          case RETURN -> returnToMenuDepartures = true;
                          
                          default -> JOptionPane.showMessageDialog(
                              null, "Please type the number representing your choice");
                        }
                      } catch (NullPointerException e) {
                        returnToMenuDepartures = true;
                      }
                    } while (!returnToMenuDepartures);
                  }
                  
                  case RETURN -> returnToMainMenu = true;
                  
                  default -> JOptionPane.showMessageDialog(
                      null, "Please type the number representing your choice");
                }
              } catch (NullPointerException e) {
                returnToMainMenu = true;
              }
            } while (!returnToMainMenu);
          }
          
          case ACCESS_REGISTER -> {
            boolean returnToMainMenu = false;
            
            do {
              try {
                switch (MenuPrinter.menuRegister()) {
                  
                  case ADD_DEPARTURE -> {
                    String trainNumber = JOptionPane.showInputDialog("Train number:");
                    String line = JOptionPane.showInputDialog("Train's line:");
                    String destination = JOptionPane.showInputDialog("Destination:");
                    String hours = JOptionPane.showInputDialog("Hours of the departure time:");
                    String minutes = JOptionPane.showInputDialog("Minutes of the departure time:");
                    String track =
                        JOptionPane.showInputDialog(
                            "Track:" + "\nLeave blank if departure does not have a track yet");
    
                    if (Objects.equals(track, "")) {
                      try {
                        trainRegister.addDeparture(trainNumber, line, destination, hours, minutes);
                        JOptionPane.showMessageDialog(
                            null, "Departure added: " + trainNumber);
                      } catch (Exception e) {
                        JOptionPane.showMessageDialog(null, e.getMessage());
                      }
                    } else {
                      try {
                        trainRegister.addDeparture(
                            trainNumber, line, destination, hours, minutes, track);
                        JOptionPane.showMessageDialog(
                            null, "Departure added: " + trainNumber);
                      } catch (Exception e) {
                        JOptionPane.showMessageDialog(null, e.getMessage());
                      }
                    }
                  }
                  
                  case REMOVE_DEPARTURE -> {
                    String trainNumber = JOptionPane.showInputDialog("Train number:");
                    
                    try {
                      trainRegister.removeDeparture(trainNumber);
                      JOptionPane.showMessageDialog(
                          null, "Departure removed: " + trainNumber);
                    } catch (Exception e) {
                      JOptionPane.showMessageDialog(null, e.getMessage());
                    }
                  }
                  
                  case GET_DEPARTURES_TO_DESTINATION -> {
                    String destination = JOptionPane.showInputDialog("Destination:");
                    StringBuilder string = new StringBuilder();
                    
                    try {
                      string.append("Departures to ").append(destination).append(":\n\n");
                      string.append(addHeaderDepartureTable());
                      ArrayList<TrainDeparture> sortedlist =
                          trainRegister.getDeparturesToDestination(destination);
                      sortedlist.forEach(train ->
                          string.append(train.departureToString()).append("\n"));
                    } catch (Exception e) {
                      JOptionPane.showMessageDialog(null, e.getMessage());
                    }
                    
                    Font font = new Font(Font.MONOSPACED, Font.PLAIN, 12);
                    JTextArea textArea = new JTextArea(string.toString());
                    textArea.setFont(font);
                    textArea.setEditable(false);
                    textArea.setLineWrap(true);
                    JScrollPane scrollPane = new JScrollPane(textArea);
                    scrollPane.setPreferredSize(new java.awt.Dimension(400, 200));
                    JOptionPane.showMessageDialog(null, scrollPane);
                  }
                  
                  case RETURN -> returnToMainMenu = true;
                  
                  default -> JOptionPane.showMessageDialog(
                      null, "Please type the number representing your choice");
                }
              } catch (NullPointerException e) {
                returnToMainMenu = true;
              }
            } while (!returnToMainMenu);
          }
          
          case EXIT -> {
            JOptionPane.showMessageDialog(
                null,
                """
              Exiting...
  
              Thank you for using the program""");
            System.exit(0);
          }
          
          default -> JOptionPane.showMessageDialog(
            null, "Please type the number representing your choice");
        }
      } catch (NullPointerException e) {
        JOptionPane.showMessageDialog(
            null,
            """
            Exiting...

            Thank you for using the program""");
        System.exit(0);
      }
    } while (true);
  }
  
  /** Static variables for the different menus. */
  private static final String SHOW_DEPARTURES = "1";
  private static final String ACCESS_CLOCK = "2";
  private static final String ACCESS_DEPARTURES = "3";
  private static final String ACCESS_REGISTER = "4";
  private static final String EXIT = "0";
  private static final String RETURN = "0";
  
  private static final String GET_TIME = "1";
  private static final String SET_TIME = "2";
  
  private static final String GET_DEPARTURE = "1";
  private static final String SET_TRACK = "2";
  private static final String SET_DELAY = "3";
  
  private static final String SET_DELAY_NEW_TIME = "1";
  private static final String SET_DELAY_DEVIATION = "2";
  
  private static final String ADD_DEPARTURE = "1";
  private static final String REMOVE_DEPARTURE = "2";
  private static final String GET_DEPARTURES_TO_DESTINATION = "3";
  
  private String addHeaderDepartureTable() {
    return "|| Time | Line  Destination      T.Nr.|Track| Delay ||\n\n";
  }
}
