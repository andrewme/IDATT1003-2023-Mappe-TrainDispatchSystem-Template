package edu.ntnu.stud;

/** This is the main class for the train dispatch application. Used to launch the program */
public class TrainDispatchApp {
  
  /**
   * Main method for the train dispatch application.
   *
   * @param args Command line arguments
   */
  public static void main(String[] args) {
    UserInterface ui = new UserInterface();
    
    ui.init();
    ui.start();
  }
}
