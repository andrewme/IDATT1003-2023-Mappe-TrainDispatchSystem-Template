package edu.ntnu.stud;

import java.time.Duration;
import java.util.regex.Pattern;

/**
 * This class contains methods for checking the validity of user input against regular expressions.
 * It also contains methods to check string length and formats the string to fit the requirements.
 * The methods are static, so they can be called without creating an instance of the class.
 *
 * @author André Merkesdal
 */
public class Checks {
  
  /** Class Variables. */
  private static final Pattern regexDestination = Pattern.compile("^[A-Za-zÆØÅæøå]+$");
  private static final Pattern regexLine = Pattern.compile("^[A-Z0-9]{1,3}$");
  private static final Pattern regexHours = Pattern.compile("[0-9]|1[0-9]|2[0-3]$");
  private static final Pattern regexMinutes = Pattern.compile("([0-9]|[0-5][0-9])$");
  private static final Pattern regexTrack = Pattern.compile("^(?:[0-9]|)$");
  private static final Pattern regexTrainNumber = Pattern.compile("[1-9]\\d{0,3}$");

  
  /**
   * Checks if the destination is valid.
   *
   * @param destination The destination to be checked
   * @return True if the train number is valid
   */
  public static boolean checkDestination(String destination) {
    return regexDestination.matcher(destination).matches();
  }

  /**
   * Checks if the line is valid.
   *
   * @param line The line to be checked
   * @return True if the train number is valid
   */
  public static boolean checkLine(String line) {
    return regexLine.matcher(line).matches();
    
  }

  /**
   * Checks if the hours are valid.
   *
   * @param hours The hours to be checked
   * @return True if the train number is valid
   */
  public static boolean checkHours(String hours) {
    return regexHours.matcher(hours).matches();
  }

  /**
   * Checks if the minutes are valid.
   *
   * @param minutes The minutes to be checked
   * @return True if the train number is valid
   */
  public static boolean checkMinutes(String minutes) {
    return regexMinutes.matcher(minutes).matches();
  }

  /**
   * Checks if the track number is valid.
   *
   * @param track The track to be checked
   * @return True if the train number is valid
   */
  public static boolean checkTrack(String track) {
    return regexTrack.matcher(track).matches();
  }

  /**
   * Checks if the train number is valid.
   *
   * @param trainNumber The train number to be checked
   * @return True if the train number is valid
   */
  public static boolean checkTrainNumber(String trainNumber) {
    return regexTrainNumber.matcher(trainNumber).matches();
  }
  
  
  /**
   * Checks and formats the destination to fit the display.
   *
   * @param destination The destination to be checked and formatted
   * @return Destination if valid
   */
  public static String checkLengthDestination(String destination) {
    if (destination.length() >= 15) {
      return destination.substring(0, 12) + "...";
    } else {
      return String.format("%-15s", destination);
    }
  }
  
  /**
   * Checks and formats the line to fit the display.
   *
   * @param line The line to be checked and formatted
   * @return The line if valid
   */
  public static String checkLengthLine(String line) {
    return String.format("%-5s", line);
  }

  /**
   * Checks and formats the track number to fit the display
   * and return empty if the track is set to -1.
   *
   * @param track The track number to be checked and formatted
   * @return The track number if valid
   */
  public static String checkLengthTrack(int track) {
    if (track == -1) {
      return "     ";
    } else {
      return String.format("  %s  ", track);
    }
  }

  /**
   * Checks and formats the train number to fit the display.
   *
   * @param trainNumber The train number to be checked and formatted
   * @return The train number if valid
   */
  public static String checkLengthTrainNumber(int trainNumber) {
    return String.format("%5s", trainNumber);
  }

  /**
   * Checks and formats the delay to fit the display and return empty if the delay is nonexistent.
   *
   * @param delay The delay to be checked and formatted
   * @return The delay if valid
   */
  public static String checkLengthDelay(Duration delay) {
    if (delay.toMinutes() == 0) {
      return String.format("%-5s", " ");
    } else {
      return String.format("%-5s", delay.toMinutes());
    }
  }
}
