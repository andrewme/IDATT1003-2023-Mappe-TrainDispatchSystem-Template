package edu.ntnu.stud;

import java.time.Duration;
import java.time.LocalTime;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

/**
 * Test class for the TrainDeparture class. The class does both positive and negative tests.
 */
class TrainDepartureTest {
    
    TrainDeparture createNewDeparture() {
        return new TrainDeparture("123", "L1", "Oslo", "12", "35");
    }
    
    /**
     * Test class for the positive tests.
     */
    @Nested
    @DisplayName("Positive tests")
    public class PositiveTests {
        
        /**
         * Positive test method for the constructor with track.
         */
        @Test
        void constructorWithTrack() {
            TrainDeparture departure = createNewDeparture();
            assertEquals(123, departure.getTrainNumber());
            assertEquals("L1", departure.getLine());
            assertEquals("Oslo", departure.getDestination());
            assertEquals(LocalTime.of(12, 35), departure.getDepartureTime());
            assertEquals(-1, departure.getTrack());
            assertEquals(Duration.ofMinutes(0), departure.getDelay());
            assertDoesNotThrow(() -> new TrainDeparture("123", "L1", "Oslo", "12", "35", "1"));
        }
        
        /**
         * Positive test method for the constructor without track.
         */
        @Test
        void constructorWithoutTrack() {
            TrainDeparture departure = createNewDeparture();
            assertEquals(123, departure.getTrainNumber());
            assertEquals("L1", departure.getLine());
            assertEquals("Oslo", departure.getDestination());
            assertEquals(LocalTime.of(12, 35), departure.getDepartureTime());
            assertEquals(-1, departure.getTrack());
            assertEquals(Duration.ofMinutes(0), departure.getDelay());
            assertDoesNotThrow(() -> new TrainDeparture("123", "L1", "Oslo", "12", "35"));
        }
        
        /**
         * Positive test method for returning the train number.
         */
        @Test
        void getTrainNumber() {
            TrainDeparture departure = createNewDeparture();
            assertEquals(123, departure.getTrainNumber());
            assertDoesNotThrow(departure::getTrainNumber);
        }

        /**
         * Positive test method for returning the departure time.
         */
        @Test
        void getDepartureTime() {
            TrainDeparture departure = createNewDeparture();
            assertEquals(LocalTime.of(12, 35), departure.getDepartureTime());
            assertDoesNotThrow(departure::getDepartureTime);
        }
        
        /**
         * Positive test method for returning the line.
         */
        @Test
        void getLine() {
            TrainDeparture departure = createNewDeparture();
            assertEquals("L1", departure.getLine());
            assertDoesNotThrow(departure::getLine);
        }
        
        /**
         * Positive test method for returning the destination.
         */
        @Test
        void getDestination() {
            TrainDeparture departure = createNewDeparture();
            assertEquals("Oslo", departure.getDestination());
            assertDoesNotThrow(departure::getDestination);
        }
        
        /**
         * Positive test method for returning the track.
         */
        @Test
        void getTrack() {
            TrainDeparture departure = createNewDeparture();
            assertEquals(-1, departure.getTrack());
            assertDoesNotThrow(departure::getTrack);
        }
        
        /**
         * Positive test method for returning the delay.
         */
        @Test
        void getDelay() {
            TrainDeparture departure = createNewDeparture();
            assertEquals(0, departure.getDelay().toMinutes());
            assertDoesNotThrow(departure::getDelay);
        }
        
        /**
         * Positive test method for getting a delayed departure time.
         */
        @Test
        void getDelayedDepartureTime() {
            TrainDeparture departure = createNewDeparture();
            departure.setDelayDeviation("10");
            assertEquals(LocalTime.of(12, 45), departure.getDelayedDepartureTime());
            assertDoesNotThrow(departure::getDelayedDepartureTime);
        }
        
        /**
         * Positive test method for setting a delayed departure time with a new time as input.
         */
        @Test
        void setDelayNewTime() {
            TrainDeparture departure = createNewDeparture();
            departure.setDelayNewTime("12", "45");
            assertEquals(LocalTime.of(12, 45), departure.getDelayedDepartureTime());
            assertDoesNotThrow(() -> departure.setDelayNewTime("12", "45"));
        }
        
        /**
         * Positive test method for setting a delayed departure time with a deviation as input.
         */
        @Test
        void setDelayDeviation() {
            TrainDeparture departure = createNewDeparture();
            departure.setDelayDeviation("10");
            assertEquals(Duration.ofMinutes(10), departure.getDelay());
            assertDoesNotThrow(() -> departure.setDelayDeviation("10"));
        }
        
        /**
         * Positive test method for setting the track.
         */
        @Test
        void setTrack() {
            TrainDeparture departure = createNewDeparture();
            departure.setTrack("1");
            assertEquals(1, departure.getTrack());
            assertDoesNotThrow(() -> departure.setTrack("1"));
        }
        
        /**
         * Positive test method for returning the departure as a string.
         */
        @Test
        void departureToString() {
            TrainDeparture departure = createNewDeparture();
            assertEquals("||12:35 | L1    Oslo            123   | |       ||", departure.departureToString());
            assertDoesNotThrow(departure::departureToString);
        }
    }
    
    /**
     * Test class for the negative tests.
     */
    @Nested
    @DisplayName("Negative tests")
    public class NegativeTests {
        /**
         * Negative test method for setting a delay with a new time as input.
         */
        @Test
        void setDelayNewTime() {
            TrainDeparture departure = createNewDeparture();
            assertThrows(IllegalArgumentException.class, () -> departure.setDelayNewTime("27", "45"));
        }
        
        /**
         * Negative test method for setting a delay with a deviation as input.
         */
        @Test
        void setDelayDeviation() {
            TrainDeparture departure = createNewDeparture();
            assertThrows(IllegalArgumentException.class, () -> departure.setDelayDeviation("-30"));
        }
        
        /**
         * Negative test method for setting the track.
         */
        @Test
        void setTrack() {
            TrainDeparture departure = createNewDeparture();
            assertThrows(IllegalArgumentException.class, () -> departure.setTrack("45"));
        }
    }
}