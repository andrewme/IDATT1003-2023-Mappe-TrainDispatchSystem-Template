package edu.ntnu.stud;

import static org.junit.jupiter.api.Assertions.*;

import java.time.LocalTime;
import java.util.NoSuchElementException;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

/**
 * Test class for TrainRegister class with both positive and negative tests.
 */
class TrainRegisterTest {
  /**
   * Creates a new TrainRegister with some departures.
   * @return a new TrainRegister with some departures.
   */
  private TrainRegister createNewRegister() {
    TrainRegister register = new TrainRegister();
    register.addDeparture("1023", "L1", "Oslo", "12", "35");
    register.addDeparture("5426", "L2", "Trondheim", "12", "30");
    register.addDeparture("5578", "L3", "Bergen", "12", "05", "1");
    register.addDeparture("101", "L4", "Stavanger", "13", "00", "2");
    return register;
  }
  
  /**
   * Test class for positive tests.
   */
  @Nested
  @DisplayName("Positive tests")
  public class PositiveTests {
    
    /**
     * Positive test for TrainRegister constructor.
     */
    @Test
    void TrainRegister() {
      assertDoesNotThrow(TrainRegister::new);
    }
    
    /**
     * Positive test for returning the main clock.
     */
    @Test
    void getMainClock() {
      TrainRegister register = createNewRegister();
      assertEquals(String.format("%2d:%2d", LocalTime.now().getHour(), LocalTime.now().getMinute()),
        register.getMainClock());
    }
    
    /**
     * Positive test for setting the main clock.
     */
    @Test
    void setMainClock() {
      TrainRegister register = createNewRegister();
      register.setMainClock("12", "35");
      assertEquals("12:35", register.getMainClock());
    }
    
    /**
     * Positive test for adding a departure with track.
     */
    @Test
    void addDepartureWithTrack() {
      TrainRegister register = createNewRegister();
      assertDoesNotThrow(() -> register.addDeparture("123", "L1", "Oslo", "12", "35", "1"));
    }
    
    /**
     * Positive test for adding a departure without track.
     */
    @Test
    void addDeparture() {
      TrainRegister register = createNewRegister();
      assertDoesNotThrow(() -> register.addDeparture("123", "L1", "Oslo", "12", "35"));
    }
    
    /**
     * Positive test for removing a departure.
     */
    @Test
    void removeDeparture() {
      TrainRegister register = createNewRegister();
      register.removeDeparture("101");
      assertDoesNotThrow(() -> register.removeDeparture("1023"));
    }
    
    /**
     * Positive test for setting a track for a departure.
     */
    @Test
    void setTrackForDeparture() {
      TrainRegister register = createNewRegister();
      register.setTrackForDeparture("1023", "2");
      assertDoesNotThrow(() -> register.setTrackForDeparture("1023", "1"));
    }
    
    /**
     * Positive test for setting a delay with new time for a departure.
     */
    @Test
    void setDelayNewTimeForDeparture() {
      TrainRegister register = createNewRegister();
      register.setDelayNewTimeForDeparture("1023", "12", "35");
      assertDoesNotThrow(() -> register.setDelayNewTimeForDeparture("1023", "12", "40"));
    }
    
    /**
     * Positive test for setting a delay with deviation for a departure.
     */
    @Test
    void setDelayDeviationForDeparture() {
      TrainRegister register = createNewRegister();
      register.setDelayDeviationForDeparture("1023", "5");
      assertDoesNotThrow(() -> register.setDelayDeviationForDeparture("1023", "10"));
    }
    
    /**
     * Positive test for returning a departure.
     */
    @Test
    void getDeparture() {
      TrainRegister register = createNewRegister();
      assertDoesNotThrow(() -> register.getDeparture("1023"));
    }
    
    /**
     * Positive test for returning departures to a specified destination.
     */
    @Test
    void getDeparturesToDestination() {
      TrainRegister register = createNewRegister();
      assertDoesNotThrow(() -> register.getDeparturesToDestination("Oslo"));
    }
    
    /**
     * Positive test for returning all departures sorted.
     */
    @Test
    void getSortedDepartures() {
      TrainRegister register = createNewRegister();
      assertDoesNotThrow(register::getSortedDepartures);
    }
  }
  
  /**
   * Test class for negative tests.
   */
  @Nested
  @DisplayName("Negative tests")
  public class NegativeTests {
    /**
     * Negative test for setting the main clock.
     */
    @Test
    void setMainClock() {
      TrainRegister register = createNewRegister();
      assertThrows(IllegalArgumentException.class, () -> register.setMainClock("34", "35"));
      assertThrows(IllegalArgumentException.class, () -> register.setMainClock("03", "40"));
    }
    
    /**
     * Negative test for adding a departure with track.
     */
    @Test
    void addDepartureWithTrack() {
      TrainRegister register = createNewRegister();
      assertThrows(IllegalArgumentException.class, () -> register.addDeparture("Numb", "L1", "Oslo", "12", "35", "1"));
    }
    
    /**
     * Negative test for adding a departure without track.
     */
    @Test
    void addDeparture() {
      TrainRegister register = createNewRegister();
      assertThrows(IllegalArgumentException.class, () -> register.addDeparture("F", "L1", "Oslo", "12", "35"));
    }
    
    /**
     * Negative test for removing a departure.
     */
    @Test
    void removeDeparture() {
      TrainRegister register = createNewRegister();
      assertThrows(IllegalArgumentException.class, () -> register.removeDeparture("7777"));
    }
    
    /**
     * Negative test for setting a track for a departure.
     */
    @Test
    void setTrackForDeparture() {
      TrainRegister register = createNewRegister();
      assertThrows(IllegalArgumentException.class, () -> register.setTrackForDeparture("1023", "10"));
      assertThrows(IllegalArgumentException.class, () -> register.setTrackForDeparture("1023", "F"));
    }
    
    /**
     * Negative test for setting a delay with new time for a departure.
     */
    @Test
    void setDelayNewTimeForDeparture() {
      TrainRegister register = createNewRegister();
      assertThrows(IllegalArgumentException.class, () -> register.setDelayNewTimeForDeparture("1023", "34", "35"));
      assertThrows(IllegalArgumentException.class, () -> register.setDelayNewTimeForDeparture("1023", "03", "40"));
    }
    
    /**
     * Negative test for setting a delay with deviation for a departure.
     */
    @Test
    void setDelayDeviationForDeparture() {
      TrainRegister register = createNewRegister();
      assertThrows(IllegalArgumentException.class, () -> register.setDelayDeviationForDeparture("1023", "F"));
    }
    
    /**
     * Negative test for returning a departure.
     */
    @Test
    void getDeparture() {
      TrainRegister register = createNewRegister();
      assertThrows(IllegalArgumentException.class, () -> register.getDeparture("1234"));
    }
    
    /**
     * Negative test for returning departures to a specified destination.
     */
    @Test
    void getDeparturesToDestination() {
      TrainRegister register = createNewRegister();
      assertThrows(IllegalArgumentException.class, () -> register.getDeparturesToDestination("Scotland"));
    }
    
    /**
     * Negative test for returning all departures sorted.
     */
    @Test
    void getSortedDepartures() {
      TrainRegister register = new TrainRegister();
      assertThrows(NoSuchElementException.class, register::getSortedDepartures);
    }
  }
}