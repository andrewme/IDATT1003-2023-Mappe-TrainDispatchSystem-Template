# Portfolio project IDATT1003 - 2023
This file uses Mark Down syntax. For more information see [here](https://www.markdownguide.org/basic-syntax/).

STUDENT NAME = André Merkesdal  
STUDENT ID = 586686

## Project description

The application Train dispatch system is an application that allows the user to maintain a train station and it's departures.
The system is text-based and rely on the user to input the correct information through different menus.


## Project structure

The project is divided into 2 packages, the main package and the test package. The main package contains the main classes
and the source code for the application. The test package contains the test classes for the application.


## Link to repository

Link: https://gitlab.stud.idi.ntnu.no/andrewme/IDATT1003-2023-Mappe-TrainDispatchSystem-Template


## How to run the project

To run the project, run the main method in the TrainDispatchApp.java class. This launches the initial menu from the UserInterface class.
Afterwards, the start method in the UserInterface class is called, which starts the program. User inputs should be typed in the input slot
where you have access to write. The program will then respond to your input. To exit the program, either type "0" in the menu
or press the "Cancel" button in the main menu.

To run the project in the terminal, run the following commands:
```
javac -d target/classes src/main/java/edu/ntnu/stud/*
java -cp target/classes edu.ntnu.stud.TrainDispatchApp
```

## How to run the tests

To run tests, run the two test classes in the test folder, TrainDepartureTest.java and TrainRegisterTest.java. 
These classes test the methods in the TrainDeparture and TrainRegister classes respectively.


## References

StackOverflow.com: https://stackoverflow.com/questions/46256058/sort-a-hashmap-by-value-in-java-8
